;;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;;; This mathematica.el defines sad-mode version 0.1
;;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;;; For programming in Strategic Accelerator Design (SAD)
;;; through GNU Emacs.
;;; by Renjun Yang (renjun.yang@cern.ch)
;;;
;;; This configuration is modified from the "madx.el" file
;;; developed by Oscar Roberto BLANCO GARCIA <orblancog@gmail.com>.
;;; Some parts were not changed (highlight of integer/floating
;;; point number, as well as syntax-table)
;;; 
;;; The "madx.el" file could be found on:
;;; https://github.com/orblancog/mad-x_syntax
;;;
;;; Highlight SAD/FFS commands of Version: 1.1.1.5k64 Updated: 2018/10/05
;;;
;;; To use this package:
;;; 
;;;   1) Save the file 'sad.el' to some convenient directory,
;;;      e.g. '~/.emacs.d/' or '~/SAD/'.
;;;
;;;   2) Insert the following in your '.emacs' initialization file
;;;      (typically under ~/ folder):
;;;
;;;      ;; Enable syntax highlighting                                                  
;;;      (global-font-lock-mode t)
;;;      (setq font-lock-maximum-decoration t)
;;;      (add-to-list 'load-path "~/SAD")
;;;      ;;(add-to-list 'load-path "~/soft/site-lisp/.emacs.d")
;;;      (autoload 'sad-mode "sad" "SAD-mode" t)
;;;      (setq auto-mode-alist (append '(("\\.\\(sad\\|n\\)$" . sad-mode))
;;;        auto-mode-alist))
;;;
;;;   3) reload emacs
;;;

(defgroup sad nil
  "Major mode for editing SAD scripts."
  :group 'languages)

(defvar sad-mode-hook nil
  "Hook for sad-model initialize nil.")

;;;; add  80 characters line
;;;; (global-whitespace-mode +1)
;(require 'whitespace)
;(setq whitespace-line-column 80) ;; limit line length
;(setq whitespace-style '(face lines-tail))
;(add-hook 'sad-mode-hook 'whitespace-mode)

(defconst sad-font-lock-keywords-face-all
  ;; sad-font-lock-keywords-programflow
  ;; 
  `((,(regexp-opt '(
       ;; flow control
       "Break"
       "Catch"
       "Check"
       "Continue"
       "Do"
       "For"
       "Goto"
       "If"
       "Label"
       "Return"
       "Switch"
       "Throw"
       "Which"
       "While"
       ;; object-orient programing and context
       "Begin"
       "BeginPackage"
       "Class"
       "End"
       "EndPackage"
       ;; scoping
       "Block"
       "Module"
       "With"
       "FFS"
       "Else"
       "ELSEIF"
       "ENDIF"
       "ABORT"
       "APPEND"
       "APP"
       "ATTR"
       "BYE"
       "CLOSE"
		   )
		  'words)
     .  font-lock-keyword-face))
  "Highlighting expressions for SAD mode (keywords-all).")

(defconst sad-font-lock-builtin-face-all
  ;; sad-font-lock-builtin-face-all
  `((,(regexp-opt '(
		   ;; input/output
       "Flush"
       "Get"
       "OpenRead"
       "OpenWrite"
       "OpenAppend"
       "PageWidth"
       "Print"
       "Read"
       "ReadString"
       "SeekFile"
       "Short"
       "StringToStream"
       "Write"
       "WriteString"
       "StandardForm"
       ;; file system
       "CopyDirectory"
       "CopyFile"
       "CreateDirectory"
       "DeleteDirectory"
       "DeleteFile"
       "DirectoryName"
       "FileByteCount"
       "FileDate"
       "FileNames"
       "FileType"       
       "RenameDirectory"
       "RenameFile"
       "SetFileDate"
       "ToFileName"
       ;; attributes
       "Attributes"
       "Clear"
       "Evaluate"
       "Head"
       "Hold"
       "Literal"
       "Protect"
       "ReleaseHold"       
       "SetAttributes"
       "Unevaluated"
       "Unprotect"
       ;; GUI widget
       "Window"
       "PanedWindow"
       "Frame"
       "LabelFrame"
       "Canvas"
       "TextLabel"
       "TextMessage"       
       "Button"
       "CheckButton"
       "RadioButton"
       "Menu"
       "OptionMenu"
       "MenuButton"
       "Entry"
       "SpinBox"
       "ListBox"
       "ScrollBar"
       "Scale"
       "TextEditor"
       ;; Graphics
       "BeamPlot"
       "ColumnPlot"
       "HistoPlot"
       "ListContourPlot"
       "ListDensityPlot"
       "ListPlot"
       "Plot"
       "Show"
       "FitPlot"
       "GeometryPlot"
       "OpticsPlot"
       "Update"
       "TkPhotoPutBlock"       
       ;; system interface
       "Directory"
       "Evironment"
       "GetEnv"
       "GetGID"
       "GetPID"
       "GetUID"
       "ProcessStatus"       
       "SetDirectory"
       "SetEnv"
       "System"
       "TemporaryName"
       "MkSecureTemp"
       "RealPath"       
       "Fork"
       "OpenShared"
       "Shared"
       "SharedSize"
       "Wait"       
       ;; utilities
       "Date"
       "DateString"
       "Definition"
       "FromDate"
       "ToDate"
       "ToDateString"
       "Pause"       
       "MemoryCheck"
       "Message"
       "Off"
       "On"
       "Sleep"
       "TimeUsed"
       "Timing"
       "TracePrint"
       "RADINT"
       "FormatType"
		   )
		  'words)
     . font-lock-builtin-face))
  "Highlighting expressions with (builtin-all).")

(defconst sad-font-lock-type-face-all
  ;; sad-font-lock-keywords-elements
  `((,(regexp-opt '(
		   ;; elements name
		   "APERT"
		   "BEND"
       "CAVI"
       "COORD"
       "DECA"
       "DODECA"
       "DRIFT"
       "MULT"
       "MARK"
       "OCT"
       "QUAD"
       "SEXT"
       "SOL"
       "TCAVI"
       "MONITOR"       
       ;; Graphics, 
       "Circle"
       "Points"
       "Rectangle"
       "Polygon"
       "Text"
       "AspectRatio"
       "DisplayFunction"
       "Detach"
       "Epilog"
       "Frame"
       "FrameClick"
       "FrameLabel"
       "FrameTicks"
       "GridLines"
       "PlotLabel"
       "PlotRange"
       "Prolog"
       "Scale"
       "TickSize"
       "FrameThickness"
       "Legend"
       "Font"
       "FontScale"
       "FrameFontScale"
       "TickFontScale"
       "LegendFontScale"
       "TextAlign"
       "TextCases"
       "TextPosition"
       "TextRotate"
       "TextSize"
       "PlotColor"
       "PointSize"
       "PointSymbol"
       "ErrorBarTickSize"
       "Dashing"
       "PointBorderColor"
       "PointTags"
       "PointSize"
       "PlotJoined"
       "Thickness"
       "FillColor"
       "Reference"
       "Orientation"
       "ColumnLabel"
       "MeshStyle"
       "Bins"
       "BinRange"
       "FitParameters"
       "Contours"
       "ColorFunction"
       "ContourColorFunction"
       "ColorScale"
       "Smoothing"
       "StepRatio"
       "Lattice"
       "LatticeRegion"
       "FrameHeight"
       "InfoLabel"
       "Names"
       "RemoveOverlap"
       "Tags"
       "Unit"
       "MaxBend"
       "PlotPoints"
       "PlotDivision"
		   )
		  'words)
     . font-lock-type-face))
  "Highlighting expressions for SAD mode (type-all).")

(defconst sad-font-lock-warning-face-all
  ;; sad-font-lock-keywords-errordef
  `((,(regexp-opt '(
		   ;; to set machine errors
		   "DELX"
		   "DELY"
       "DL"
       "DTHETA"
       "DK"
       "DDK"
       "PUT"
       "ADD"       
		   )
		  'words)
     . font-lock-warning-face))
  "Highlighting expressions for SAD mode (warning-all).")

(defconst sad-font-lock-special_operators
  ;; sad-font-lock-special_operators
  ;; special symbols alike Mathematica format
  `((,(regexp-opt '(
		   ;; special_operators in FFS
       "#"
       "##"
       "?"
       "::"
       "@"
       "++"
       "--"
       "/@"
       "//@"
       "@@"
       "^"
       "=="
       "<>"
       "<"
       ">"
       ">="
       "=>"
       "<="
       "=<"
       "==="
       "<==>"
       "&"
       "&&"
       "|"
       "||"
       ".."
       "..."
       ":"
       "->"
       ":>"
       "/."
       "//."
       "+="
       "-="
       "*="
       "/="
       ;; "/"
       "//"
       ":="
       "^="
       "^:="
       "=."
       ";"
       "%"
       "_"
       "__"
       "___"
       ;; character-string
       "¥n"
       "¥r"
       "¥t"
       "¥"
       "¥nnn"
		   )
		  t)
     . font-lock-warning-face))
  "Highlighting expressions for SAD mode (special-operators).")

(defconst sad-font-lock-constant-face-all
  ;; sad-font-lock-keywords-constants
  `((,(regexp-opt '(
       "True"
       "False"
       "Infinity"
       "INF"
       "I"
		   "PI"
		   "E"
       "Degree"
       "GoldenRatio"
       "EulerGamma"
       ;; Physical-constants
       "SpeedOfLight"
       "FineStructureConstant"
       "ElectronCharge"
       "ElectronMass"
       "ElectronRadius"
       "ProtonMass"
       "ProtonRadius"
       "SIMu0"
       "SIEpsilon0"
       "BoltzmannConstant"       
		   )
		  'words)
     . font-lock-constant-face))
  "Highlighting expressions for SAD mode (constant-all).")

(defconst sad-font-lock-flags
  ;; sad-font-lock-flags
  `((,(regexp-opt '(
       ;; flags       
       "ABSW"
       "BIPOL"
       "CALC4D"
       "CALC6D"
       "CELL"
       "CMPLOT"
       "COD"
       "CODPLOT"
       "CONV"
       "CONVCASE"
       "CTIME"
       "DAMPONLY"
       "DAPERT"
       "DIFFRES"
       "ECHO"
       "EMIOUT"
       "FFSPRMPT"
       "FIXSEED"
       "FLUC"
       "GAUSS"
       "GEOCAL"
       "GEOFIX"
       "HALFRES"
       "IDEAL"
       "INS"
       "INTRA"
       "INTRES"
       "JITTER"
       "LOG"
       "LWAKE"
       "MOVESEED"
       "PHOTONS"
       "PRSVCASE"
       "PSPAC"
       "QUIET"
       "RAD"
       "RADCOD"
       "RADLIGHT"
       "RADTAPER"
       "REAL"
       "RELW"
       "RFSW"
       "RING"
       "SELFCOD"
       "SORG"
       "SPAC"
       "STABLE"
       "SUMRES"
       "TRPT"
       "TWAKE"
       "UNIFORM"
       "UNIPOL"
       "UNSTABLE"
       "WSPAC"
		   )
		  t)
     . font-lock-constant-face))
  "Highlighting expressions for SAD mode (special_constants).")

(defconst sad-font-lock-doc-face-all
  ;; Beam and BeamLine commands
  `((,(regexp-opt '(
       ;; FFS functions
       "BeamMatrix"
       "CalculateOptics"
       "DynamicApertureSurvey"
       "Element"
       "Emittance"       
       "FitValue"
       "FitWeight"
       "GeoBase"
       "LINE"
       "OptimizeOptics"
       "OrbitGeo"
       "RadiationField"       
       "RadiationSpectrum"
       "SymplecticJ"
       "SetElement"
       "TrackParticles"
       "Twiss"
       "VariableRange"       
       "SynchroBetaEmittance"
       "TouschekLifetime"
       "WakeFunction"       
       ;; beam-line-functions:
       "BeamLine"
       "BeamLineName"
       "ExtractBeamLine"
       "PrintBeamLine"
       "WriteBeamLine"
       ;; others
       "CALCULATE"
       "CAL"
       "Chromaticity"
       "CHRO"
       "COUPLE"
       "COUP"
       "DISPALY"
       "DISP"
       "ACCELERATION"
       "ALL"
       "BEAM"
       "DUMPOPTICS"
       ;"D"
       "GEOMETRY"
       "G"
       "OGEOMETRY"
       "OG"
       "ORBIT"
       "PHYSICAL"
       "P"
       "RMATRIX"
       "R"
       "Draw"
       "DUMP"
       "EMIT"
       "EXECUTE"
       "EXEC"
       "EXPAD"
       "AccelerateParticles"
       "FREE"
       "FITPOINTS"
       "FIX"
       "GO"
       ; "GEO"
       "INPUT"
       "IN"
       "MATRIX"
       "MAT"
       "MEASURE"
       "MEA"
       "ORG"
       "OUTPUT"
       "OUT"
       "Terminate"
       "TERM"
       "Recover"
       "REC"
       "Reject"
       "REJ"
       "Repeat"
       "REP"
       "SAVE"
       "SEED"
       "SHOW"
       "SPLIT"
       "STATUS"
       "STAT"
       "STOP"
       "TDR"
       "TYPE"
       "UNTIL"
       "USE"
       "VARIBLES"
       "VAR"
       "VARY"
       "VISIT"
       "$FORM"
       "CorrectOrbit"
       "MakeBump"
       "CorrectOrbitBump"
		   )
		  'words)
     . font-lock-doc-face))
  "Highlighting expressions for SAD mode (doc-all).")

(defconst sad-font-lock-function-name-face-all
  ;; sad-font-lock-keywords-functions
  ;; Functions in FFS
  `((,(regexp-opt '(;;  font-lock-function-name-face
       "Arccos"
       "ArcCosh"
       "ArcSin"
       "ArcSinh"
       "ArcTan"
       "ArcTanh"
       "Cos"
       "Cosh"
       "Exp"
       "Log"
       "Sin"
       "Sinh"
       "Sqrt"
       "Tan"
       "Tanh"
       ;; part special functions
       "BesselI"
       "BesselJ"
       "BesselK"
       "BesselY"
       "BesselJZero"
       "Erf"
       "Erfc"
       "Factorial"
       "Gamma"
       "LogGamma"
       "LogGamma1"
       "GammaRegularized"
       "GammaRegularizedP"
       "GaussianCoulomb"
       "GaussianCoulombU"
       "GaussianCoulombFitted"
       "LegendreP"       
       ;; numerical functions
       "Abs"
       "Ceiling"
       "Floor"
       "Max"
       "Min"
       "MinMax"
       "Mod"
       "Negative"
       "NonNegative"
       "Positive"       
       "Round"
       "Sign"
       "FractionalPart"
       ;; matrix-operation
       "Det"
       "Eigensystem"
       "IdentityMatrix"
       "Inner"
       "LinearSolve"
       "Outer"
       "SingularValues"
       "Transpose"
       ;; random number
       "GaussRandom"
       "Random"
       "SeedRandom"
       "ParabolaRandom"
       "ListRandom"
       ;; complex
       "Arg"
       "Complex"
       "ComplexQ"
       "Conjugate"
       "Im"
       "Re"
       ;; Rational
       "Rational"
       "FromRational"
       "ContinuedFraction"
       "FromContinuedFraction"
       "SmallPrime"       
       "Numerator"
       "Denominator"
       ;; Fourier-transformation
       "Fourier"
       "InverseFourier"
       ;; data manupilation
       "FindRoot"
       "Fit"
       "NIntegrate"
       "PolynomialFit"
       "Spline"
       ;; calculus
       "D"
       "NIntegrate"
       "DownhillSimplex"
       ;; list-manipulation
       "Append"
       "Complement"
       "Delete"
       "Depth"
       "Difference"
       "Dimensions"
       "Drop"
       "Extract"
       "Flatten"
       "FlattenAt"
       "HeldPart"
       "Insert"
       "Intersection"
       "Join"
       "Length"
       "List"
       "Part"
       "Partition"
       "Prepend"
       "Product"
       "Range"
       "ReplacePart"
       "Reset"
       "Reverse"
       "Rev"
       "RotateLeft"
       "RotateRight"
       "Select"       
       "Sort"
       "Sum"
       "Take"
       "Table"
       "Union"
       ;; character strings
       "FromCharacterCode"
       "CharacterPosition"
       "Characters"
       "DigitQ"
       "LetterQ"
       "StringDrop"       
       "StringFill"
       "StringInsert"
       "StringLength"
       "StringPosition"
       "StringTrim"       
       "Symbol"
       "SymbolName"
       "ToCharacteCode"
       "ToLowerCase"
       "ToUpperCase"
       "ToExpressio"
       ;; Functional operations
       "Apply"
       "Cases"
       "Count"
       "DeleteCases"
       "Identity"
       "FixedPoint"
       "FixedPointList"       
       "Fold"
       "Function"
       "Level"
       "Map"
       "MapAll"
       "MapAt"
       "MapIndexed"
       "MapThread"
       "Nest"
       "Position"       
       "Replace"
       "Scan"
       "ScanThread"
       "SelectCases"
       "SwitchCases"
       "Thread"
       ;; tests
       "AtomQ"
       "ComplexQ"
       "DirectoryQ"
       "EvenQ"
       "FileQ"
       "MatchQ"
       "MatrixQ"
       "MemberQ"
       "NearlySameQ"       
       "Negative"
       "NonNegative"
       "NumberQ"
       "OddQ"
       "Order"
       "Positive"
       "RealQ"
       "StringQ"
       "VectorQ"       
       "BoundQ"
       "FBoundQ"
       ;; string commands
       "FromCharacterCode"
       "StringFill"
       "StringJoin"
       "StringLength"
       "StringMatchQ"
       "StringPart"
       "StringPosition"
       "StringReplace"
       "StringTrim"
       "Symbol"
       "ToCharacterCode"
       "ToExpression"
       "ToString"
       ;; color
       "Black"
       "White"
       "Red"
       "Green"
       "Blue"
       "Yellow"
       "Magneta"
       "Cyan"
		   )
		  'words)
     . font-lock-function-name-face))
  "Highlighting expressions for SAD mode (name-all)." )

(defconst sad-font-lock-variable-name-face-all
  ;; sad-font-lock-keywords-variables_sad
  `((,(concat (regexp-opt '(
			    ;; parameters
			    "CHI1"
			    "CHI2"
			    "CHI3"
			    "CHI4"
          ;; to character elements
          "AE1"
          "AE2"
          "ANGLE"
          "BOUND"
          "DDP"
          "DISFRIN"
          "DISRAD"
          "DIR"
          "DPHI"
          "DPX"
          "DPY"
          "DROTATE"
          "DVOLT"
          "DX"
          "DY"
          "E1"
          "E2"
          "F1"
          "F2"
          "FB1"
          "FB2"
          "FREQ"
          "FRINGE"
          "GEO"
          "HARM"
          "K0"
          "K1"
          "K2"
          "K3"
          "K4"
          "K5"
          "K6"
          "K7"
          "K8"
          "K9"
          "K10"
          "K11"
          "K12"
          "K13"
          "K14"
          "K15"
          "K16"
          "K17"
          "K18"
          "K19"
          "K20"
          "K21"
          "L"
          "OFFSET"
          "PHI"
          "RADIUS"
          "ROTATE"
          "SK0"
          "SK1"
          "SK2"
          "SK3"
          "SK4"
          "SK5"
          "SK6"
          "SK7"
          "SK8"
          "SK9"
          "SK10"
          "SK11"
          "SK12"
          "SK13"
          "SK14"
          "SK15"
          "SK16"
          "SK17"
          "SK18"
          "SK19"
          "SK20"
          "SK21"
          "V02"
          "V1"
          "V11"
          "VOLT"
          ;; optical functions
          "AX"
          "BX"
          "NX"
          "AY"
          "BY"
          "NY"
          "EX"
          "EPX"
          "EY"
          "EPY"
          "R1"
          "R2"
          "R3"
          "R4"
          "DETR"
          "DZ"
          "AZ"
          "BZ"
          "NZ"
          "ZX"
          "ZPX"
          "ZY"
          "ZPY"
          "PEX"
          "PEPX"
          "PEY"
          "PEPY"
          "TRX"
          "TRY"
          "LENG"
          "GX"
          "GY"
          "GZ"
          ;; special varibles
          "CASE"
          "CHARGE"
          "CONVERGENCE"
          "DAPWIDTH"
          "DP"
          "DP0"
          "DTSYNCH"
          "EFFRFFREQ"
          "EFFVC"
          "EFFVCRATIO"
          "ElementValues"
          "EMITX"
          "EMITXE"
          "EMITY"
          "EMITYE"
          "EMITZ"
          "EMITZE"
          "ExponentOfResidual"
          "FitFunction"
          "FSHIFT"
          "GCUT"
          "InitialOrbits"
          "LOSSAMPL"
          "LOSSDZ"
          "MatchingAmplitude"
          "MatchingResidual"
          "MASS"
          "MINCOUP"
          "MOMENTUM"
          "NBUNCH"
          "NetResidual"
          "NP"
          "NPARA"
          "OffMomentumWeigth"
          "OMEGA0"
          "OpticsEpilog"
          "OpticsProlog"
          "PBUNCH"
          "PhotoList"
          "PHICAV"
          "SIGE"
          "SIGZ"
          "StabilityLevel"
          "STACKSIZ"
          "TITLE"          
			    )
			  'words)
	      ;; some variables already optimized
	      ;; "\\|\\<RE[1-6][1-6]\\>"
	      ;; "\\|\\<TM?[1-6][1-6][1-6]\\>"
	      ;; "\\|\\<BETA[1-3][1-3]P?\\>"
	      ;; "\\|\\<ALFA[1-3][1-3]P?\\>"
	      ;; "\\|\\<GAMA[1-3][1-3]P?\\>"
	      ;; "\\|\\<DISP[1-4]P[1-3]\\>"
	      ;; "\\|\\<EIGN[1-6][1-6]\\>"
	      ;"\\|\\<RM?[1-6][1-6]\\>"
              )
     . font-lock-variable-name-face))
  "Highlighting expressions for SAD mode (variable-name-all).")


(defconst sad-font-lock-intfp-name-face-all
  ;; for integers and floating point numbers
  (list
   '("\\<\\(\\([0-9]+\\.?[0-9]*\\|\\.[0-9]+\\)\\([eE][+-]?\\([0-9]+\\.?[0-9]*\\|[0-9]*\\.[0-9]+\\)\\)?\\)\\>"
     . font-lock-keyword-face))
  "Highlighting expresssions for SAD mode (integers and floats).")


(defconst sad-font-lock-keywords-4
  (append
   sad-font-lock-flags
   sad-font-lock-special_operators
   sad-font-lock-keywords-face-all
   sad-font-lock-constant-face-all
   sad-font-lock-function-name-face-all
   sad-font-lock-type-face-all
   sad-font-lock-variable-name-face-all
   sad-font-lock-builtin-face-all
   sad-font-lock-warning-face-all
   sad-font-lock-doc-face-all
   sad-font-lock-intfp-name-face-all)
 "Balls-out highlighting in SAD mode.")

(defvar sad-font-lock-keywords sad-font-lock-keywords-4
  "Default highlighting expressions for SAD mode.")

(defvar sad-mode-syntax-table
  (let ((sad-mode-syntax-table (make-syntax-table)))
	
  ;; This is added so entity names with underscores and dots can be more easily parsed
  (modify-syntax-entry ?_ "w" sad-mode-syntax-table)
  (modify-syntax-entry ?. "w" sad-mode-syntax-table)
	
  ;;  Comment styles are similar to C++
  ;; (modify-syntax-entry ?/ ". 124 b" sad-mode-syntax-table)
  (modify-syntax-entry ?* ". 23" sad-mode-syntax-table)
  (modify-syntax-entry ?\n "> b" sad-mode-syntax-table)
  (modify-syntax-entry ?! "< b" sad-mode-syntax-table)
  (modify-syntax-entry ?' "|" sad-mode-syntax-table)
  ;; Wolfram Language style comment “(* … *)”
  (modify-syntax-entry ?\( ". 1" sad-mode-syntax-table)
  (modify-syntax-entry ?\) ". 4" sad-mode-syntax-table)
  (modify-syntax-entry ?* ". 23" sad-mode-syntax-table)  
     sad-mode-syntax-table)
  "Syntax table for `sad-mode'.")

;;
(define-derived-mode sad-mode fundamental-mode "sad"
  "Major mode for editing SAD scripts."
  (make-local-variable 'font-lock-defaults)
  (setq font-lock-defaults '(sad-font-lock-keywords nil t))
  ;; Setting up Imenu
  (setq imenu-generic-expression nil)
  (setq imenu-prev-index-position-function nil)
  (setq imenu-extract-index-name-function nil)
  ;;  (imenu-create-index-function)

  ;; Set up search
  (setq case-fold-search t)
  )

;; Enable syntax highlighting
(global-font-lock-mode t)
;;
(setq font-lock-maximum-decoration t)
;;
(provide 'sad-mode)
