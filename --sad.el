;;; For programing in SAD, refering to madx.el configuration ...
;;  by Renjun Yang (renjun.yang@cern.ch)
;;  26/10/2018


 (defgroup sad nil
 "Major mode to edit SAD files scripts in emacs"
 :group 'languages
 )

(defvar sad-mode-hook nil)

;; define key map

;; autoload 
(add-to-list 'auto-mode-alist '("\\.zad\\'" . sad-mode))

;; add  80 characters line
;;(global-whitespace-mode +1)
(require 'whitespace)
(setq whitespace-line-column 160) ;; limit line length
(setq whitespace-style '(face lines-tail))
(add-hook 'madx-mode-hook 'whitespace-mode)

;; Syntax highlighting

;; (defconst sad-font-lock-keywords-1
;;  (list
;;   '("\\<\\(ABSW\\|BIPOL\\|(C\\(ALC4D\\|ALC6D\\|ELL\\|MPLOT|\\OD|\\ODPLOT|\\ONV|\\ONVCASE\\)\\|WSPAC)\\>" . font-lock-keyword-face))
;;  "xx experssions for sad mode")


(defconst sad-font-lock-keywords-face-all
  ;; sad-font-lock-keywords-programflow
  (list
   '("\\<\\(ELSE\\(?:IF\\)?\\|IF\\|MACRO\\|WHILE\\)\\>"
   .  font-lock-keyword-face)
  )
  "Highlighting expressions for SAD mode.")


(defconst sad-font-lock-keywords-4
  (append
   sad-font-lock-special_constants
   sad-font-lock-special_operators
   sad-font-lock-keywords-face-all
   sad-font-lock-constant-face-all
   sad-font-lock-function-name-face-all
   sad-font-lock-type-face-all
   sad-font-lock-variable-name-face-all
   sad-font-lock-builtin-face-all
   sad-font-lock-warning-face-all
   sad-font-lock-doc-face-all   
  )
 "Balls-out highlighting in SAD mode.")

(defvar sad-font-lock-keywords sad-font-lock-keywords-4
  "Default highlighting expressions for SAD mode.")

(defvar sad-mode-syntax-table
  (let ((sad-mode-syntax-table (make-syntax-table)))
	
    ; This is added so entity names with underscores can be more easily parsed
	(modify-syntax-entry ?_ "w" sad-mode-syntax-table)
	(modify-syntax-entry ?. "w" sad-mode-syntax-table)
	
	;  Comment styles are same as C++
	(modify-syntax-entry ?/ ". 124 b" sad-mode-syntax-table)
	(modify-syntax-entry ?* ". 23" sad-mode-syntax-table)
	(modify-syntax-entry ?\n "> b" sad-mode-syntax-table)
	(modify-syntax-entry ?! "< b" sad-mode-syntax-table)
	(modify-syntax-entry ?' "|" sad-mode-syntax-table)
	sad-mode-syntax-table)
  "Syntax table for SAD-mode")

;;; ### autoload  
(defun sad-mode ()
  "Major mode for editing MAD-X script files"
  (interactive)
  (kill-all-local-variables)
  (setq mode-name "SAD")
  (setq major-mode 'sad-mode)
;;   (setq comment-start "!")
  (set-syntax-table sad-mode-syntax-table)
  (make-local-variable 'font-lock-defaults)
  (setq font-lock-defaults '(sad-font-lock-keywords nil t))
;; Set up search
  (add-hook 'sad-mode-hook
     (lambda ()  (setq case-fold-search t)))
  (run-hooks 'sas-mode-hook)
  )

(provide 'madx-mode)

;;; madx-mode.el ends here


;(defun sad-mode ()
;  "Major mode for editing SAD files/scripts"
;  (interactive)
;  (kill-all-local-variables)
;  ;; (set-syntax-table sad-mode-syntax-table)
;  ;; (use-local-map sad-mode-map)
;  (set (make-local-variable 'font-lock-defaults) '(sad-font-lock-keywords))
;  
;  (setq major-mode 'sad-mode)
;  (setq mode-name "SAD")
;  (run-hooks 'sad-mode-hook)  ;; call sad mode hooks
;)
;
;(provide 'sad-mode)
